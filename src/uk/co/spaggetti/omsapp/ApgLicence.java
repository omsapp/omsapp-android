package uk.co.spaggetti.omsapp;

import android.content.Context;
import de.psdev.licensesdialog.licenses.License;

public class ApgLicence extends License {

	private static final long serialVersionUID = 2653707076500850637L;

	@Override
	public String getName() {
		return "APG Licence";
	}

	@Override
	public String getSummaryText(Context context) {
		return getContent(context, R.raw.apg_licence);
	}

	@Override
	public String getFullText(Context context) {
		return getContent(context, R.raw.apg_licence);
	}

	@Override
	public String getVersion() {
		return "";
	}

	@Override
	public String getUrl() {
		return "";
	}
}
