package uk.co.spaggetti.omsapp;

import uk.co.spaggetti.omsapp.ui.SeekBarToolTip;
import uk.co.spaggetti.omsapp.ui.UiUtils;
import uk.co.spaggetti.password_utils.PasswordUtils;
import android.animation.ArgbEvaluator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import de.psdev.licensesdialog.LicenseResolver;
import de.psdev.licensesdialog.LicensesDialog;

public class MainActivity extends Activity {

	private ViewGroup container;
	private EditText passwordEditText;
	private Button generateButton;
	private SeekBar lengthSeekBar;
	private ProgressBar strengthProgressBar;
	private ImageButton copyButton;

	private CheckBox lowercaseSwitch;
	private CheckBox uppercaseSwitch;
	private CheckBox numbersSwitch;
	private CheckBox specialSwitch;
	private CheckBox pronounceableSwitch;
	private SeekBarToolTip lengthToolTip;

	/**
	 * Minimum length of the generated password.
	 */
	private static final int MIN_PASSWORD_LEN = 4;
	/**
	 * Maximum length of the generated password.
	 */
	private static final int MAX_PASSWORD_LEN = 12;

	/**
	 * Maximum size of the generation alphabet.
	 */
	private static final int MAX_ALPHABET_SIZE = 90;

	/**
	 * Max entropy of a generated password, in bits.
	 */
	private final float MAX_PASSWORD_ENTROPY = (float) (Math.log(Math.pow(MAX_ALPHABET_SIZE,
			MAX_PASSWORD_LEN)) / Math.log(2.0));

	/**
	 * Enabled character sets, as configured by the UI.
	 */
	private int passwordCharSets;

	/**
	 * Password length, as configured by the UI.
	 */
	private int passwordLength;

	/**
	 * Whether pronounceable mode is enabled, as configured by the UI.
	 */
	private boolean passwordPronounceable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setDisplayShowTitleEnabled(false);
		UiUtils.forceShowOverflowMenu(this);

		setContentView(R.layout.activity_main);

		container = (ViewGroup) findViewById(R.id.container);

		passwordEditText = (EditText) findViewById(R.id.edittext_password);
		passwordEditText.addTextChangedListener(passwordEditTextWatcher);

		copyButton = (ImageButton) findViewById(R.id.button_copy);
		copyButton.setOnClickListener(copyButtonOnClickListener);
		copyButton.getDrawable().setColorFilter(getResources().getColor(R.color.omsapp_grey),
				Mode.SRC_IN);

		generateButton = (Button) findViewById(R.id.button_generate);
		generateButton.setOnClickListener(generateButtonOnClickListener);

		lengthSeekBar = (SeekBar) findViewById(R.id.seekbar_length);
		lengthSeekBar.setOnSeekBarChangeListener(lengthSeekBarChangeListener);

		strengthProgressBar = (ProgressBar) findViewById(R.id.progressbar_password_strength);

		lowercaseSwitch = (CheckBox) findViewById(R.id.switch_lowercase);
		uppercaseSwitch = (CheckBox) findViewById(R.id.switch_uppercase);
		numbersSwitch = (CheckBox) findViewById(R.id.switch_numbers);
		specialSwitch = (CheckBox) findViewById(R.id.switch_special);
		pronounceableSwitch = (CheckBox) findViewById(R.id.switch_pronounceable);
		lowercaseSwitch.setOnCheckedChangeListener(switchOnCheckedChangeListener);
		uppercaseSwitch.setOnCheckedChangeListener(switchOnCheckedChangeListener);
		numbersSwitch.setOnCheckedChangeListener(switchOnCheckedChangeListener);
		specialSwitch.setOnCheckedChangeListener(switchOnCheckedChangeListener);
		pronounceableSwitch.setOnCheckedChangeListener(switchOnCheckedChangeListener);

		lengthToolTip = (SeekBarToolTip) findViewById(R.id.seekbartooltip_length);

		passwordCharSets = getEnabledCharacterSetsFromControls();
		passwordLength = getPasswordLengthFromControls();
		passwordPronounceable = getPronounceableEnabledFromControls();

		if (canGenerate()) {
			generate();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.about) {
			startActivity(new Intent(this, AboutActivity.class));
			return true;
		} else if (item.getItemId() == R.id.licences) {
			LicenseResolver.registerLicense(new ApgLicence());
			new LicensesDialog(this, R.raw.licences, false, true).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private TextWatcher passwordEditTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			updateStrengthMeter();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private OnClickListener copyButtonOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String password = passwordEditText.getText().toString();
			ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			ClipData clip = ClipData.newPlainText("password", password);
			clipboardManager.setPrimaryClip(clip);
			Toast.makeText(MainActivity.this, getString(R.string.clipboard_copied_message),
					Toast.LENGTH_SHORT).show();
		}
	};

	private final OnClickListener generateButtonOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			generate();
		}
	};

	private final OnSeekBarChangeListener lengthSeekBarChangeListener = new OnSeekBarChangeListener() {

		// The tooltip is only used when getThumb() is available
		private final boolean tooltipEnabled = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN);

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			if (tooltipEnabled) {
				lengthToolTip.show();
			}
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			if (tooltipEnabled) {
				lengthToolTip.hide();
			}
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			int passwordLength = getPasswordLengthFromControls();
			boolean passwordLengthChanged = passwordLength != MainActivity.this.passwordLength;
			MainActivity.this.passwordLength = passwordLength;

			if (passwordLengthChanged) {
				if (tooltipEnabled) {
					updateLengthTooltipText(passwordLength);
				}
				updateControlStates();
			}

			if (tooltipEnabled) {
				updateLengthTooltipPosition();
			}

			if (canGenerate()) {
				generate();
			}
		}

		private void updateLengthTooltipText(int passwordLength) {
			lengthToolTip.setText("Length (" + passwordLength + ")");
		}

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void updateLengthTooltipPosition() {
			Rect thumbBounds = lengthSeekBar.getThumb().getBounds();
			int newLeft = thumbBounds.left + thumbBounds.width() - (lengthToolTip.getWidth() / 2);
			int newRight = newLeft + lengthToolTip.getWidth();

			// Constrain to bounds of container
			if (newLeft < container.getLeft()) {
				lengthToolTip.setX(container.getLeft());
			} else if (newRight > container.getRight()) {
				lengthToolTip.setX(container.getRight() - lengthToolTip.getWidth());
			} else {
				lengthToolTip.setX(newLeft);
			}
		}
	};

	private final OnCheckedChangeListener switchOnCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			passwordCharSets = getEnabledCharacterSetsFromControls();
			passwordPronounceable = pronounceableSwitch.isChecked();
			updateControlStates();
			if (canGenerate()) {
				generate();
			}
		}
	};

	private boolean canGenerate() {
		return (passwordCharSets != 0) && passwordLength > 0;
	}

	private void generate() {
		String password;
		if (passwordPronounceable) {
			password = PasswordUtils.generatePronouceable(passwordLength, passwordCharSets);
		} else {
			password = PasswordUtils.generateRandom(passwordLength, passwordCharSets);
		}
		passwordEditText.setText(password);
	}

	private void updateStrengthMeter() {
		float strength = PasswordUtils.estimateStrength(passwordEditText.getText().toString())
				/ MAX_PASSWORD_ENTROPY;
		strengthProgressBar.setProgress((int) (strength * 100.0f));

		ArgbEvaluator argbEvaluator = new ArgbEvaluator();
		Interpolator interpolator = new DecelerateInterpolator(1.0f);
		strength = interpolator.getInterpolation(strength);
		Integer color = (Integer) argbEvaluator.evaluate(strength, Color.RED, getResources()
				.getColor(R.color.omsapp_green));
		strengthProgressBar.getProgressDrawable().setColorFilter(color, Mode.SRC_IN);
	}

	private void updateControlStates() {
		// Lowercase switch must be ON in pronounceable mode
		if (pronounceableSwitch.isChecked()) {
			lowercaseSwitch.setChecked(true);
		}

		// Lowercase switch cannot be togged in pronounceable mode
		lowercaseSwitch.setEnabled(!pronounceableSwitch.isChecked());

		// Can only generate passwords when a character set is enabled
		generateButton.setEnabled(canGenerate());
	}

	private int getEnabledCharacterSetsFromControls() {
		int characterSets = 0;
		characterSets |= lowercaseSwitch.isChecked() ? PasswordUtils.CharSet.LOWER_CASE : 0;
		characterSets |= uppercaseSwitch.isChecked() ? PasswordUtils.CharSet.UPPER_CASE : 0;
		characterSets |= numbersSwitch.isChecked() ? PasswordUtils.CharSet.NUMBER : 0;
		characterSets |= specialSwitch.isChecked() ? PasswordUtils.CharSet.SPECIAL : 0;
		return characterSets;
	}

	private int getPasswordLengthFromControls() {
		float fraction = (float) lengthSeekBar.getProgress() / (float) lengthSeekBar.getMax();
		return MIN_PASSWORD_LEN + (int) ((MAX_PASSWORD_LEN - MIN_PASSWORD_LEN) * fraction);
	}

	private boolean getPronounceableEnabledFromControls() {
		return pronounceableSwitch.isChecked();
	}
}
