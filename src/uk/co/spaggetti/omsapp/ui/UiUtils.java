package uk.co.spaggetti.omsapp.ui;

import java.lang.reflect.Field;

import android.app.Activity;
import android.os.Build;
import android.view.ViewConfiguration;

public class UiUtils {

	/**
	 * Forces the action bar overflow button to show on devices with hardware
	 * menu buttons. Only affects pre-4.4 devices.
	 */
	public static void forceShowOverflowMenu(Activity activity) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			try {
				ViewConfiguration config = ViewConfiguration.get(activity);
				Field menuKeyField = ViewConfiguration.class
						.getDeclaredField("sHasPermanentMenuKey");

				if (menuKeyField != null) {
					menuKeyField.setAccessible(true);
					menuKeyField.setBoolean(config, false);
				}
			} catch (Exception e) {
			}
		}
	}
}
