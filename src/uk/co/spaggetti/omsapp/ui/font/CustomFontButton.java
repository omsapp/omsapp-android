package uk.co.spaggetti.omsapp.ui.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomFontButton extends Button {

	public CustomFontButton(Context context) {
		super(context);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontUtils.applyCustomFont(this);
	}
}
