package uk.co.spaggetti.omsapp.ui.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomFontEditText extends EditText{

	public CustomFontEditText(Context context) {
		super(context);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontUtils.applyCustomFont(this);
	}
}
