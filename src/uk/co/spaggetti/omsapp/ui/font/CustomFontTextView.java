package uk.co.spaggetti.omsapp.ui.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomFontTextView extends TextView {

	public CustomFontTextView(Context context) {
		super(context);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontUtils.applyCustomFont(this);
	}
}
