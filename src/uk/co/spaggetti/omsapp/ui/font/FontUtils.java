package uk.co.spaggetti.omsapp.ui.font;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontUtils {

	private static final String FONT_NAME = "pt_sans_caption_regular.ttf";

	private static Typeface cached;

	private static final Typeface getTypeFace(Context context) {
		if (cached == null) {
			cached = Typeface.createFromAsset(context.getAssets(), FONT_NAME);
		}
		return cached;
	}

	/**
	 * Applies the theme font to a view.
	 */
	public static void applyCustomFont(TextView view) {
		view.setTypeface(getTypeFace(view.getContext()));
	}
}
