package uk.co.spaggetti.omsapp.ui.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class CustomFontCheckBox extends CheckBox {

	public CustomFontCheckBox(Context context) {
		super(context);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontUtils.applyCustomFont(this);
	}

	public CustomFontCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontUtils.applyCustomFont(this);
	}
}
