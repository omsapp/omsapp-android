package uk.co.spaggetti.omsapp.ui;

import uk.co.spaggetti.omsapp.R;
import uk.co.spaggetti.omsapp.ui.font.FontUtils;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Small panel-like view that fades in/out like the ListView fast scroll
 * control.
 */
public class SeekBarToolTip extends FrameLayout {

	private int shortDuration;
	private int longDuration;
	private TextView textView;

	public SeekBarToolTip(Context context) {
		this(context, null);
	}

	public SeekBarToolTip(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SeekBarToolTip(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(getContext()).inflate(R.layout.view_seekbar_tooltip, this);
		shortDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
		longDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
		setVisibility(View.INVISIBLE);
		setAlpha(0.0f);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		textView = (TextView) findViewById(R.id.label);
		FontUtils.applyCustomFont(textView);
	}

	/**
	 * Fades the tooltip in.
	 */
	public void show() {
		setVisibility(View.VISIBLE);
		animate().alpha(1.0f).setStartDelay(0).setDuration(shortDuration).start();
	}

	/**
	 * Hides the tooltip after a short delay.
	 */
	public void hide() {
		animate().alpha(0.0f).setStartDelay(longDuration).setDuration(longDuration).start();
	}

	/**
	 * Sets the text displayed by the tooltip.
	 */
	public void setText(String text) {
		textView.setText(text);
	}
}
