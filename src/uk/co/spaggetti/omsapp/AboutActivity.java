package uk.co.spaggetti.omsapp;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		setCreditText();
	}

	private void setCreditText() {
		TextView textView = (TextView) findViewById(R.id.credit);
		try {
			String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			String s = String.format("%s v%s\n%s", getString(R.string.app_name), version,
					getString(R.string.about_credit));
			textView.setText(s);
		} catch (NameNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
